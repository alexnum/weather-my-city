import { Component, Input, OnInit } from '@angular/core';

import { Clima } from './general_information';

import { WeatherService } from './get-information.service';


@Component({
  selector: 'info-tempo-cidade',
  templateUrl: './info-cidade.component.html',
  styleUrls: [ './info-cidade.component.scss' ],
})
export class InformacaoCidade {
  @Input() clima: Clima;
}
