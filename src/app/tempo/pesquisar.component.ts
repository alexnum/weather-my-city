import { Component, Input } from '@angular/core';

import { Clima } from './general_information';

import { WeatherService } from './get-information.service';

import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'pesquisar-cidade',
  templateUrl: './pesquisar.component.html',
  styleUrls: ['./pesquisar.component.scss']
})

export class PesquisarCidadeComponent {
  private readonly notifier: NotifierService;

  public url = '/city/cities';
  public query = '';

  title = 'Clima agora';
  @Input() cidade : string;
  informacoesClima: Clima;
  pesquisando = false;

  constructor(private weatherService: WeatherService, notifierService: NotifierService) {
    this.notifier = notifierService;
  }

  handleResultSelected (result) {
    this.onProcuraCidade(result);
    this.query = result;
  }
  
  onProcuraCidade(cidade: string) {
    this.limparDados(true);
    this.weatherService.getWeatherCity(cidade)
      .then(clima => {
        this.limparDados(false);
        this.informacoesClima = clima;
        if(clima == null){
          this.notifier.notify( 'error', 'Cidade não encontrada' );
        }
      });
  }

  limparDados(pesquisando: boolean) {
    if (pesquisando) {
      this.informacoesClima = null;
    }
    this.pesquisando = pesquisando;
  }
}
