interface ICoordenadas {
  lat: number;
  lon: number
}

export class Clima {
  cidade: string;
  estadoCeu: string;
  temperatura: number;
  pressao: number;
  umidade: number;
  temperatura_maxima: number;
  temperatura_minima: number;
  velocidade_vento: number;
  visibilidade: number;
  coordenadas: ICoordenadas;
}
