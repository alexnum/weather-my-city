import {Injectable} from '@angular/core';

import { RESPONSE_CITY } from './mocks';

import { Clima } from './general_information';


@Injectable()
export class WeatherService {

  getWeatherCity(cidade: string): Promise<Clima> {
    return this.searchWeatherCity(cidade).then((dados) => {
      let filteredWeather = null;
      if(dados['cod'] == 200){
        filteredWeather = this.filtraDados(dados)
      }
      return filteredWeather;
    }
    );
  }

  searchWeatherCity(cidade: string): Promise<object> {
    const urlBase = 'http://api.openweathermap.org/data/2.5/weather';
    const token = '8d042d8571555866cde060b2757c5aef';
    let cidadeEncoded = encodeURIComponent(cidade);
    let url =`${urlBase}?q=${cidadeEncoded}&APPID=${token}`;

    return fetch(url)
    .then(response => response.json())
    .catch(function() {
      console.log('error');
    });
  }

  filtraDados(data: object): Clima {
    let info = new Clima();
    info.estadoCeu = data['weather'][0]["description"];
    info.temperatura = data['main']['temp'];
    info.pressao = data['main']['pressure'];
    info.umidade = data['main']['humidity'];
    info.temperatura_maxima = data['main']['temp_min'];
    info.temperatura_minima = data['main']['temp_max'];
    info.velocidade_vento = data['wind']['speed'];
    info.visibilidade = data['visibility'];
    info.coordenadas = data['coord'];
    info.cidade = data['name'];
    return info;
  }

}
