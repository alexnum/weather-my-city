# WeatherMyCity

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.10.

##Requisitos

  - Docker 18.03.1-ce
  - Docker-compose 1.21.2
  - Make 4.1
  - Angular 4 (https://v4.angular.io/)
  - Angular/CLI 1.4.10
  - Npm

**OBS**:

O projeto foi feito e executado sempre no sistema operacional Ubuntu 17.10, não sendo testado em outros ambientes.

## Rodando local

Apos instalar o angular/cli, entrar no diretório e instalar as dependências do node.

```bash
$: npm install
```

Rodando a aplicacao

```bash
$: make deploy_local
```

**A aplicação esta sendo executada na porta 9001**
