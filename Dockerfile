FROM ubuntu:18.04

RUN apt update
RUN apt upgrade -y

FROM nginx:1.15.0-alpine

COPY ./dist /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
