/* File Created by Alessandro Herculano
-------------2019-------------
*/

var express = require('express');
var bodyParser = require('body-parser');
var request = require('request-promise-native');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/cities', async function (req, res) {
  let query = encodeURIComponent(req.query.q);
  let uri = `http://gd.geobytes.com/AutoCompleteCity?q=${query}`;
  try{
    let response = await request({uri, json: true});
    res.jsonp([
      req.query.q, 
      response.map(item => [item, 0])
    ]);
  }catch(err){
    res.status(500).send('Could not retrieve cities');
  }
});

app.listen(3000, function () {
  console.log('Cities Services Listening on port 3000!!!');
});

